# title scrolls

adds title scrolls that display under a username

scroll texture modified from [rpg items retro pack](https://blodyavenger.itch.io/rpg-items-retro-pack)

## data format
data driven titles are placed in the `titles` folder of a data pack, then added to uncommon rare or epic titles by 
setting the `Title` nbt tag to the id of the title to use

```json
{
  "ribbon_color": "#FF0000",
  "title_text": {
    "text": "your text here in json text format",
    "color": "dark_purple",
    "italic": true
  }
}
```

`"ribbon_color"` is a hex code or color name

`"title_text"` uses the [json text format](https://minecraft.tools/en/json_text.php)

jij using jitpack if you want