package repulica.titlescrolls;

import java.nio.ByteBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import repulica.titlescrolls.api.TitleEffect;
import repulica.titlescrolls.api.TitleEffects;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.particle.ParticleTypes;
import net.minecraft.util.Identifier;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.client.rendering.v1.ColorProviderRegistry;
import net.fabricmc.fabric.api.network.ClientSidePacketRegistry;

public class TitleScrollsClient implements ClientModInitializer {

	private static final ByteBuffer buf = BufferUtils.createByteBuffer(128);

	private static final long[]  GREY_35 = { 0x2222222255555555L, 0x8080808055555555L, 0x2222222255555555L, 0x0888088855555555L, 0x2222222255555555L, 0x8080808055555555L, 0x2222222255555555L, 0x0808080855555555L, 0x2222222255555555L, 0x8080808055555555L, 0x2222222255555555L, 0x0888088855555555L, 0x2222222255555555L, 0x8080808055555555L, 0x2222222255555555L, 0x0808080855555555L };
	private static final long[]  GREY_40 = { 0xaaaaaaaa11111111L, 0xaaaaaaaa54545454L, 0xaaaaaaaa11111111L, 0xaaaaaaaa45444544L, 0xaaaaaaaa11111111L, 0xaaaaaaaa54545454L, 0xaaaaaaaa11111111L, 0xaaaaaaaa44454445L, 0xaaaaaaaa11111111L, 0xaaaaaaaa54545454L, 0xaaaaaaaa11111111L, 0xaaaaaaaa45444544L, 0xaaaaaaaa11111111L, 0xaaaaaaaa54545454L, 0xaaaaaaaa11111111L, 0xaaaaaaaa44454445L };
	private static final long[]  GREY_45 = { 0xaaaaaaaa44444444L, 0xaaaaaaaa55555555L, 0xaaaaaaaa54545454L, 0xaaaaaaaa55555555L, 0xaaaaaaaa44444444L, 0xaaaaaaaa55555555L, 0xaaaaaaaa54445444L, 0xaaaaaaaa55555555L, 0xaaaaaaaa44444444L, 0xaaaaaaaa55555555L, 0xaaaaaaaa54545454L, 0xaaaaaaaa55555555L, 0xaaaaaaaa44444444L, 0xaaaaaaaa55555555L, 0xaaaaaaaa54445444L, 0xaaaaaaaa55555555L };
	private static final long[]  GREY_50 = { 0xaaaaaaaa55555555L, 0xaaaaaaaa55555555L, 0xaaaaaaaa55555555L, 0xaaaaaaaa55555555L, 0xaaaaaaaa55555555L, 0xaaaaaaaa55555555L, 0xaaaaaaaa55555555L, 0xaaaaaaaa55555555L, 0xaaaaaaaa55555555L, 0xaaaaaaaa55555555L, 0xaaaaaaaa55555555L, 0xaaaaaaaa55555555L, 0xaaaaaaaa55555555L, 0xaaaaaaaa55555555L, 0xaaaaaaaa55555555L, 0xaaaaaaaa55555555L };
	private static final long[]  GREY_55 = { 0x55555555aaaaaaaaL, 0xd5d5d5d5aaaaaaaaL, 0x55555555aaaaaaaaL, 0xddddddddaaaaaaaaL, 0x55555555aaaaaaaaL, 0xd5ddd5ddaaaaaaaaL, 0x55555555aaaaaaaaL, 0xddddddddaaaaaaaaL, 0x55555555aaaaaaaaL, 0xd5d5d5d5aaaaaaaaL, 0x55555555aaaaaaaaL, 0xddddddddaaaaaaaaL, 0x55555555aaaaaaaaL, 0xd5ddd5ddaaaaaaaaL, 0x55555555aaaaaaaaL, 0xddddddddaaaaaaaaL };
	private static final long[]  GREY_60 = { 0x55555555eeeeeeeeL, 0x55555555babbbabbL, 0x55555555eeeeeeeeL, 0x55555555ababababL, 0x55555555eeeeeeeeL, 0x55555555bbbabbbaL, 0x55555555eeeeeeeeL, 0x55555555ababababL, 0x55555555eeeeeeeeL, 0x55555555babbbabbL, 0x55555555eeeeeeeeL, 0x55555555ababababL, 0x55555555eeeeeeeeL, 0x55555555bbbabbbaL, 0x55555555eeeeeeeeL, 0x55555555ababababL };
	private static final long[]  GREY_65 = { 0x55555555bbbbbbbbL, 0x55555555fefefefeL, 0x55555555bbbbbbbbL, 0x55555555efeeefeeL, 0x55555555bbbbbbbbL, 0x55555555fefefefeL, 0x55555555bbbbbbbbL, 0x55555555efefefefL, 0x55555555bbbbbbbbL, 0x55555555fefefefeL, 0x55555555bbbbbbbbL, 0x55555555efeeefeeL, 0x55555555bbbbbbbbL, 0x55555555fefefefeL, 0x55555555bbbbbbbbL, 0x55555555efefefefL };

	private static final long[][] MIX = { GREY_35, GREY_40, GREY_45, GREY_50, GREY_55, GREY_60, GREY_65, GREY_65, GREY_60, GREY_55, GREY_50, GREY_45, GREY_40, GREY_35 };

	@Override
	public void onInitializeClient() {
		ColorProviderRegistry.ITEM.register((stack, tintIndex) -> {
			if (tintIndex == 1) return ((TitleScrollItem) stack.getItem()).getRibbonColor(stack);
			return 0xFFFFFF;
		}, TitleScrolls.TITLE_SCROLL, TitleScrolls.UNCOMMON_TITLE_SCROLL, TitleScrolls.RARE_TITLE_SCROLL, TitleScrolls.EPIC_TITLE_SCROLL);
		ClientSidePacketRegistry.INSTANCE.register(TitleScrolls.TITLE_PACKET, (context, buf) -> TitleManager.INSTANCE.fromPacket(buf));
		TitleEffects.INSTANCE.registerEffect(new Identifier(TitleScrolls.MODID, "none"), TitleEffect.NONE);
		TitleEffects.INSTANCE.registerEffect(new Identifier(TitleScrolls.MODID, "ender"),
				((stack, slot, matrixStack, vertexConsumer, light, model, player, headYaw, headPitch) ->
						player.world.addParticle(ParticleTypes.PORTAL, player.getParticleX(0.5D),
								player.getRandomBodyY() - 0.25D, player.getParticleZ(0.5D),
								(player.getRandom().nextDouble() - 0.5D) * 2.0D,
								-player.getRandom().nextDouble(),
								(player.getRandom().nextDouble() - 0.5D) * 2.0D)
				)
		);
		TitleEffects.INSTANCE.registerEffect(new Identifier(TitleScrolls.MODID, "ashen"),
				((stack, slot, matrixStack, vertexConsumer, light, model, player, headYaw, headPitch) ->
						player.world.addParticle(ParticleTypes.ASH, player.getParticleX(0.5D),
								player.getRandomBodyY() - 0.25D, player.getParticleZ(0.5D),
								(player.getRandom().nextDouble() - 0.5D) * 2.0D,
								-player.getRandom().nextDouble(),
								(player.getRandom().nextDouble() - 0.5D) * 2.0D)
				)
		);
		TitleEffects.INSTANCE.registerEffect(new Identifier(TitleScrolls.MODID, "pulse"),
				((stack, slot, matrices, vertexConsumer, light, model, player, headYaw, headPitch) -> {
					if (MinecraftClient.getInstance().currentScreen != null) return;
					matrices.push();
					matrices.scale(0.95f, 0.95f, 0.95f);
					GL11.glEnable(GL11.GL_POLYGON_STIPPLE);
					int currentFrame = (int) (System.nanoTime() / 100_000_000) % 14;
					buf.asLongBuffer().put(MIX[currentFrame]);
					GL11.glPolygonStipple(buf);
					model.render(matrices,
							vertexConsumer.getBuffer(RenderLayer.getEntityTranslucentCull(player.getSkinTexture())),
							light,
							light,
							1f,
							1f,
							1f,
							1f
					);
					GL11.glDisable(GL11.GL_POLYGON_STIPPLE);
					matrices.pop();
				})
		);
	}
}
