package repulica.titlescrolls;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nullable;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.minecraft.network.PacketByteBuf;
import net.minecraft.resource.JsonDataLoader;
import net.minecraft.resource.ResourceManager;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Text;
import net.minecraft.text.TextColor;
import net.minecraft.util.Identifier;
import net.minecraft.util.JsonHelper;
import net.minecraft.util.profiler.Profiler;

import net.fabricmc.fabric.api.resource.IdentifiableResourceReloadListener;

public class TitleManager extends JsonDataLoader implements IdentifiableResourceReloadListener {
	public static final TitleManager INSTANCE = new TitleManager();
	private static final Logger LOGGER = LogManager.getLogger();
	private static final Identifier ID = new Identifier(TitleScrolls.MODID, "title_manager");

	private final Map<Identifier, Title> titles = new HashMap<>();

	private TitleManager() {
		super(new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().setLenient().create(), "titles");
	}

	@Override
	protected void apply(Map<Identifier, JsonElement> loader, ResourceManager manager, Profiler profiler) {
		titles.clear();
		for (Identifier id : loader.keySet()) {
			JsonObject json = JsonHelper.asObject(loader.get(id), "title");
			TextColor color = TextColor.parse(JsonHelper.getString(json, "ribbon_color"));
			JsonObject text = JsonHelper.getObject(json, "title_text");
			String effect = JsonHelper.getString(json, "render_effect", "titlescrolls:none");
			titles.put(id, new Title(Text.Serializer.fromJson(text), color, new Identifier(effect)));
		}
		LOGGER.info("Loaded {} titles", titles.size());
	}

	public void toPacket(PacketByteBuf buf) {
		buf.writeVarInt(titles.size());
		for (Identifier id : titles.keySet()) {
			buf.writeIdentifier(id);
			Title title = titles.get(id);
			buf.writeText(title.getText());
			buf.writeVarInt(title.getColor());
			buf.writeIdentifier(title.getEffect());
		}
	}

	public void fromPacket(PacketByteBuf buf) {
		titles.clear();
		int length = buf.readVarInt();
		for (int i = 0; i < length; i++) {
			Identifier id = buf.readIdentifier();
			Text text = buf.readText();
			int color = buf.readVarInt();
			Identifier effect = buf.readIdentifier();
			titles.put(id, new Title(text, color, effect));
		}
	}

	public Title getTitle(Identifier id) {
		return titles.getOrDefault(id, Title.NONE);
	}

	@Override
	public Identifier getFabricId() {
		return ID;
	}

	public static class Title {
		public static final Title NONE = new Title(new LiteralText("Blank Scroll"), 0xFFFFFF,
				new Identifier(TitleScrolls.MODID, "none"));

		private final Text text;
		private final int color;
		private final Identifier effect;

		public Title(Text text, @Nullable TextColor color, Identifier effect) {
			this.text = text;
			if (color == null) this.color = 0xFF0000;
			else this.color = color.getRgb();
			this.effect = effect;
		}

		public Title(Text text, int color, Identifier effect) {
			this.text = text;
			this.color = color;
			this.effect = effect;
		}

		public Text getText() {
			return text;
		}

		public int getColor() {
			return color;
		}

		public Identifier getEffect() {
			return effect;
		}
	}
}
