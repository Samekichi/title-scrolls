package repulica.titlescrolls.mixin;

import dev.emi.trinkets.api.TrinketsApi;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import repulica.titlescrolls.TitleScrollItem;

import net.minecraft.client.network.AbstractClientPlayerEntity;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.entity.EntityRenderDispatcher;
import net.minecraft.client.render.entity.LivingEntityRenderer;
import net.minecraft.client.render.entity.PlayerEntityRenderer;
import net.minecraft.client.render.entity.model.PlayerEntityModel;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;

@Environment(EnvType.CLIENT)
@Mixin(PlayerEntityRenderer.class)
public abstract class MixinPlayerEntityRenderer extends LivingEntityRenderer<AbstractClientPlayerEntity, PlayerEntityModel<AbstractClientPlayerEntity>> {
	public MixinPlayerEntityRenderer(EntityRenderDispatcher dispatcher, PlayerEntityModel<AbstractClientPlayerEntity> model, float shadowRadius) {
		super(dispatcher, model, shadowRadius);
	}

	@Inject(method = "renderLabelIfPresent", at = @At(value = "INVOKE", target = "Lnet/minecraft/client/network/AbstractClientPlayerEntity;getScoreboard()Lnet/minecraft/scoreboard/Scoreboard;"), cancellable = true)
	private void injectTitleRender(AbstractClientPlayerEntity player, Text text, MatrixStack matrices, VertexConsumerProvider vertices, int i, CallbackInfo info) {
		ItemStack stack = TrinketsApi.getTrinketComponent(player).getStack("misc", "title");
		if ((stack.getItem() instanceof TitleScrollItem)) {
			Text title = ((TitleScrollItem) stack.getItem()).getTitle(stack);
			matrices.push();
			matrices.scale(0.75f, 0.75f, 0.75f);
			matrices.translate(0f, 0.6f, 0f);
			super.renderLabelIfPresent(player, title, matrices, vertices, i);
			matrices.pop();
			matrices.translate(0d, 0.1225d, 0d);
//			matrices.translate(0.0d, (9.0f * 1.15f * 0.025f), 0.0d);
			super.renderLabelIfPresent(player, text, matrices, vertices, i);
			matrices.pop();
			info.cancel();
		}
	}
}
