package repulica.titlescrolls.mixin;

import java.util.Iterator;

import io.netty.buffer.Unpooled;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;
import repulica.titlescrolls.TitleManager;
import repulica.titlescrolls.TitleScrolls;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.ClientConnection;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.network.packet.s2c.play.SynchronizeRecipesS2CPacket;
import net.minecraft.server.PlayerManager;
import net.minecraft.server.network.ServerPlayerEntity;

import net.fabricmc.fabric.api.network.ServerSidePacketRegistry;

@Mixin(PlayerManager.class)
public class MixinPlayerManager {
	@Inject(method = "onDataPacksReloaded", at = @At(value = "INVOKE", target = "Lnet/minecraft/server/network/ServerRecipeBook;sendInitRecipesPacket(Lnet/minecraft/server/network/ServerPlayerEntity;)V"), locals = LocalCapture.CAPTURE_FAILEXCEPTION)
	private void injectPacketSend(CallbackInfo info, SynchronizeRecipesS2CPacket packet, Iterator<PlayerEntity> playerIterator, ServerPlayerEntity player) {
		PacketByteBuf buf = new PacketByteBuf(Unpooled.buffer());
		TitleManager.INSTANCE.toPacket(buf);
		ServerSidePacketRegistry.INSTANCE.sendToPlayer(player, TitleScrolls.TITLE_PACKET, buf);
	}

	@Inject(method = "onPlayerConnect", at = @At(value = "INVOKE", target = "Lnet/minecraft/server/PlayerManager;sendCommandTree(Lnet/minecraft/server/network/ServerPlayerEntity;)V"))
	private void injectPacketSend(ClientConnection connection, ServerPlayerEntity player, CallbackInfo info) {
		PacketByteBuf buf = new PacketByteBuf(Unpooled.buffer());
		TitleManager.INSTANCE.toPacket(buf);
		ServerSidePacketRegistry.INSTANCE.sendToPlayer(player, TitleScrolls.TITLE_PACKET, buf);
	}
}
