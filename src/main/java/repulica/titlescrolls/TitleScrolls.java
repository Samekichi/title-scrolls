package repulica.titlescrolls;

import dev.emi.trinkets.api.TrinketSlots;

import net.minecraft.entity.EntityType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.loot.condition.KilledByPlayerLootCondition;
import net.minecraft.loot.entry.ItemEntry;
import net.minecraft.loot.function.SetNbtLootFunction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.resource.ResourceType;
import net.minecraft.util.Identifier;
import net.minecraft.util.Rarity;
import net.minecraft.util.registry.Registry;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.loot.v1.FabricLootPoolBuilder;
import net.fabricmc.fabric.api.loot.v1.event.LootTableLoadingCallback;
import net.fabricmc.fabric.api.resource.ResourceManagerHelper;

public class TitleScrolls implements ModInitializer {
	public static final String MODID = "titlescrolls";
	public static final Identifier TITLE_PACKET = new Identifier(MODID, "update_packets");

	public static Item TITLE_SCROLL;
	public static Item UNCOMMON_TITLE_SCROLL;
	public static Item RARE_TITLE_SCROLL;
	public static Item EPIC_TITLE_SCROLL;

	@Override
	public void onInitialize() {
		TrinketSlots.addSlotGroup("misc", "title");
		TrinketSlots.addSlot("misc", "title", new Identifier(MODID, "textures/item/title_scroll_slot.png"));
		TITLE_SCROLL = Registry.register(Registry.ITEM, new Identifier(MODID, "title_scroll"),
				new TitleScrollItem(new Item.Settings()
						.group(ItemGroup.MISC)
						.maxCount(1)));
		UNCOMMON_TITLE_SCROLL = Registry.register(Registry.ITEM, new Identifier(MODID, "uncommon_title_scroll"),
				new SpecialTitleScrollItem(new Item.Settings()
						.group(ItemGroup.MISC)
						.maxCount(1)
						.rarity(Rarity.UNCOMMON)));
		RARE_TITLE_SCROLL = Registry.register(Registry.ITEM, new Identifier(MODID, "rare_title_scroll"),
				new SpecialTitleScrollItem(new Item.Settings()
						.group(ItemGroup.MISC)
						.maxCount(1)
						.rarity(Rarity.RARE)));
		EPIC_TITLE_SCROLL = Registry.register(Registry.ITEM, new Identifier(MODID, "epic_title_scroll"),
				new SpecialTitleScrollItem(new Item.Settings()
						.group(ItemGroup.MISC)
						.maxCount(1)
						.rarity(Rarity.EPIC)));
		ResourceManagerHelper.get(ResourceType.SERVER_DATA).registerReloadListener(TitleManager.INSTANCE);
		CompoundTag dragonTag = new CompoundTag();
		dragonTag.putString("Title", "titlescrolls:dragon_slayer");
		CompoundTag witherTag = new CompoundTag();
		witherTag.putString("Title", "titlescrolls:ashen");
		LootTableLoadingCallback.EVENT.register((resourceManager, lootManager, id, builder, setter) -> {
			if (id.equals(EntityType.ENDER_DRAGON.getLootTableId())) {
				builder.withPool(FabricLootPoolBuilder.builder()
						.withEntry(
								ItemEntry.builder(RARE_TITLE_SCROLL)
										.build()
						).withFunction(
								SetNbtLootFunction.builder(dragonTag)
									.build()
						).withCondition(KilledByPlayerLootCondition.builder()
								.build()
						).build());
			} else if (id.equals(EntityType.WITHER.getLootTableId())) {
				builder.withPool(FabricLootPoolBuilder.builder()
						.withEntry(
								ItemEntry.builder(RARE_TITLE_SCROLL)
										.build()
						).withFunction(
								SetNbtLootFunction.builder(witherTag)
										.build()
						).withCondition(KilledByPlayerLootCondition.builder()
								.build()
						).build());
			}
		});
		//TODO: source for empowered scrolls?
	}
}
