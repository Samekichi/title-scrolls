package repulica.titlescrolls;

import java.util.List;

import dev.emi.trinkets.api.TrinketsApi;
import repulica.titlescrolls.api.TitleEffects;

import net.minecraft.client.item.TooltipContext;
import net.minecraft.client.network.AbstractClientPlayerEntity;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.entity.model.PlayerEntityModel;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Identifier;
import net.minecraft.world.World;

import net.fabricmc.fabric.api.util.NbtType;

public class SpecialTitleScrollItem extends TitleScrollItem {
	public SpecialTitleScrollItem(Settings settings) {
		super(settings);
	}

	@Override
	public Text getTitle(ItemStack stack) {
		if (stack.getOrCreateTag().contains("Title", NbtType.STRING)) {
			Identifier id = new Identifier(stack.getOrCreateTag().getString("Title"));
			return TitleManager.INSTANCE.getTitle(id).getText();
		}
		return super.getTitle(stack);
	}

	@Override
	public int getRibbonColor(ItemStack stack) {
		if (stack.getOrCreateTag().contains("Title", NbtType.STRING)) {
			Identifier id = new Identifier(stack.getOrCreateTag().getString("Title"));
			return TitleManager.INSTANCE.getTitle(id).getColor();
		}
		return super.getRibbonColor(stack);
	}

	@Override
	public boolean canInsert(ItemStack stack) {
		return true;
	}

	@Override
	public void appendTooltip(ItemStack stack, World world, List<Text> tooltip, TooltipContext context) {
		super.appendTooltip(stack, world, tooltip, context);
		tooltip.add(new TranslatableText("message.titlescrolls.title", getTitle(stack)));
	}

	@Override
	public boolean hasGlint(ItemStack stack) {
		return true;
	}

	@Override
	public void render(String slot, MatrixStack matrixStack, VertexConsumerProvider vertexConsumer, int light,
					   PlayerEntityModel<AbstractClientPlayerEntity> model, AbstractClientPlayerEntity player,
					   float headYaw, float headPitch) {
		ItemStack stack = TrinketsApi.getTrinketComponent(player).getStack(slot);
		if (stack.getOrCreateTag().contains("Title", NbtType.STRING)) {
			Identifier id = new Identifier(stack.getOrCreateTag().getString("Title"));
			Identifier effect = TitleManager.INSTANCE.getTitle(id).getEffect();
			TitleEffects.INSTANCE.getEffect(effect).render(stack, slot, matrixStack, vertexConsumer, light, model,
					player, headYaw, headPitch);
		}
	}
}
