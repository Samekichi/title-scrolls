package repulica.titlescrolls.api;

import net.minecraft.client.network.AbstractClientPlayerEntity;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.entity.model.PlayerEntityModel;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.item.ItemStack;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;

@Environment(EnvType.CLIENT)
@FunctionalInterface
public interface TitleEffect {
	/**
	 * a no-op title effect
	 */
	TitleEffect NONE = ((stack, slot, matrixStack, vertexConsumer, light, model, player, headYaw, headPitch) -> {});

	/**
	 * render an effect when a certain title is worn
	 * @param stack the stack of the title scroll
	 * @param slot the slot the title is in
	 * @param matrixStack the matrixstack used to render
	 * @param vertexConsumer the vertexconsumer used to render
	 * @param light the light level to render with
	 * @param model the player model
	 * @param player the player
	 * @param headYaw the player's head yaw
	 * @param headPitch the player's head pitch
	 */
	void render(ItemStack stack, String slot, MatrixStack matrixStack, VertexConsumerProvider vertexConsumer, int light,
				PlayerEntityModel<AbstractClientPlayerEntity> model, AbstractClientPlayerEntity player,
				float headYaw, float headPitch);
}
