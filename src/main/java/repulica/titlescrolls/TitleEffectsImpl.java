package repulica.titlescrolls;

import java.util.HashMap;
import java.util.Map;

import repulica.titlescrolls.api.TitleEffects;
import repulica.titlescrolls.api.TitleEffect;

import net.minecraft.util.Identifier;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;

@Environment(EnvType.CLIENT)
public class TitleEffectsImpl implements TitleEffects {

	private final Map<Identifier, TitleEffect> effects = new HashMap<>();

	@Override
	public void registerEffect(Identifier id, TitleEffect effect) {
		effects.put(id, effect);
	}

	@Override
	public TitleEffect getEffect(Identifier id) {
		return effects.getOrDefault(id, TitleEffect.NONE);
	}
}
